package com.example.pixelcity.activities

import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import com.example.pixelcity.screens.fullVPager.FullVPagerViewMvc
import com.example.pixelcity.screens.fullVPager.FullVPagerViewMvcImpl
import com.example.pixelcity.services.LocServices
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class FullViewPagerActivity : AppCompatActivity(), FullVPagerViewMvc.PagerListener {


    private lateinit var viewMvc: FullVPagerViewMvcImpl
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private val storagePermissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,
    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
    //TODO: Ask emad about this (a3mlo set bekam)
    private var myStorageRPermission = 12
    private var myStorageEPermission = 12
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewMvc = FullVPagerViewMvcImpl(LayoutInflater.from(this), null)
        setContentView(viewMvc.getRootView())
       viewMvc.registerListener(this)
        supportActionBar?.hide()
        searchPhotos(page!!)
    }


    override fun onPhotoDownloadClicked(drawable: Drawable) {
        saveImageClicked(drawable)
    }


    private fun searchPhotos(page: Int){
        LocServices.searchPhotos(SearchResultActivity.location?.latitude?.toFloat(), SearchResultActivity.location?.longitude?.toFloat(), page){ response, error ->
            if(error == null){
                if(response!!.photos.total == 0){
                    Toast.makeText(this, "This area has no photos.", Toast.LENGTH_LONG).show()
                    val intentMap = Intent(this, MapsActivity::class.java)
                    startActivity(intentMap)
                }
                else{
                    val items = response.photos.photo
                    viewMvc.bindRv(items, index!!)
                }
            }

            else{
                Toast.makeText(this, error, Toast.LENGTH_LONG).show()
                Log.e("Server-Error", error)
            }
        }
    }

    private fun saveToGallery(drawable : Drawable) {
        val bitmap = (drawable as BitmapDrawable).bitmap
        val imageUri : Uri? = Uri.fromFile(File(saveImageToStorage(bitmap)))
        saveImageToStorage(bitmap)
        Toast.makeText(this, "Saved Successfully", Toast.LENGTH_LONG).show()
    }

    private fun saveImageClicked(drawable: Drawable) {
        Toast.makeText(this, "entered saveImage", Toast.LENGTH_LONG).show()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*if (ContextCompat.checkSelfPermission(this, storagePermissions[0]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, storagePermissions[1]) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this@FullViewPagerActivity, android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    MaterialAlertDialogBuilder(this)
                        .setTitle("Permission is needed")
                        .setMessage("Grant permission to read and write to Storage.")
                        .setPositiveButton("Grant Permission") { dialog, _ ->
                            dialog.dismiss()
                            askForStoragePermission() }
                        .create()
                        .show()

                } else {
                    askForStoragePermission()
                }

            } else {*/ saveToGallery(drawable)/* }*/
        } else {  saveToGallery(drawable) }
    }

    override fun onPhotoShareClicked(drawable: Drawable) {
        var fOut: FileOutputStream? = null
        try{
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
            val directoryPath = File(this.externalCacheDir, "sharedImages")
            directoryPath.mkdir()
            val file = File(directoryPath, "${timeStamp}.png")
            fOut = FileOutputStream(file)
            val bitmap = drawable.toBitmap()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fOut)
            //start Intent
            val shareIntent = Intent(Intent.ACTION_SEND)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Intent.EXTRA_STREAM, FileProvider.getUriForFile(this@FullViewPagerActivity,
                    "com.example.pixelcity.provider", //(use your app signature + ".provider" )
                    file))
                .setType("image/png")
            startActivity(shareIntent)
            file.deleteOnExit()
        }catch (e: Exception){
            e.printStackTrace()
        }finally {
            try {
                fOut?.flush()
                fOut?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }

    private fun saveImageToStorage(bitmap: Bitmap?): String {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        //saves in folder then images, saves in DCIM private, saves in same but public
//        val contextWrapper = ContextWrapper(this)
//        val directory = contextWrapper.getDir("Images", MODE_PRIVATE)
//        val directory = getDir(Environment.DIRECTORY_DCIM, Context.MODE_PRIVATE)
        //working on old and new phones
        val directory = this.getExternalFilesDir(Environment.DIRECTORY_DCIM)

        val myPath = File(directory, "img_${timeStamp}.png")
        var fos: FileOutputStream? = null
        try{
            Log.e("7ammo",myPath.toString())
            fos = FileOutputStream(myPath)
            bitmap?.compress(Bitmap.CompressFormat.PNG, 100, fos)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.flush()
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return directory!!.absolutePath
    }


    private fun askForStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActivityCompat.requestPermissions(this@FullViewPagerActivity, storagePermissions, myStorageRPermission)
        }
    }


    fun shareImageClicked(url: String) {
        Toast.makeText(this, "Share was just clicked", Toast.LENGTH_LONG).show()
    }

    override fun onReachRecyclerEnd() {
        page = page!! + 1
        searchPhotos(page!!)
    }

    override fun onRecyclerClicked(index: Int) {
    }

    companion object{
        var index: Int? = 0
        var page: Int? = 1
    }

}
