package com.example.pixelcity.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import com.example.pixelcity.screens.mainMap.MapsViewMvc
import com.example.pixelcity.screens.mainMap.MapsViewMvcImpl

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class MapsActivity : AppCompatActivity(),
    MapsViewMvc.MapsListener {


    private lateinit var viewMvc: MapsViewMvcImpl
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        viewMvc = MapsViewMvcImpl(LayoutInflater.from(this), null)

        setContentView(viewMvc.getRootView())

        viewMvc.registerListener(this)

    }

    override fun onMapClicked(latLng: LatLng) {
        viewMvc.dropPin(latLng)
    }

    override fun onPlaceSelected(latLng: LatLng, title: String) {
        viewMvc.dropPin(latLng, title)
    }

    override fun onMarkerClicked(marker: Marker) {
        SearchResultActivity.location = marker.position
        val intentResultActivity = Intent(this, SearchResultActivity::class.java)
        startActivity(intentResultActivity)
    }
}
