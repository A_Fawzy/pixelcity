package com.example.pixelcity.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.pixelcity.screens.searchResult.SearchResultViewMvc
import com.example.pixelcity.screens.searchResult.SearchResultViewMvcImpl
import com.example.pixelcity.services.LocServices
import com.example.pixelcity.utilities.PER_PAGE
import com.google.android.gms.maps.model.LatLng

class SearchResultActivity : AppCompatActivity(),
    SearchResultViewMvc.SearchResultListener{

    lateinit var viewMvc: SearchResultViewMvcImpl
    var page = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewMvc = SearchResultViewMvcImpl(LayoutInflater.from(this), null)
        setContentView(viewMvc.getRootView())
        viewMvc.registerListener(this)
        searchPhotos(page)
    }

    private fun searchPhotos(page: Int){
        LocServices.searchPhotos(location?.latitude?.toFloat(), location?.longitude?.toFloat(), page){ response, error ->
            if(error == null){
                if(response!!.photos.total == 0){
                    Toast.makeText(this, "This area has no photos.", Toast.LENGTH_LONG).show()
                    val intentMap = Intent(this, MapsActivity::class.java)
                    startActivity(intentMap)
                }
                else{
                    val items = response.photos.photo
                    viewMvc.bindRv(items)
                }
            }

            else{
                Toast.makeText(this, error, Toast.LENGTH_LONG).show()
                Log.e("Server-Error", error)
            }
        }
    }

    override fun onRecyclerClicked(index: Int) {
        val intentFullViewPager = Intent(this, FullViewPagerActivity::class.java)
        FullViewPagerActivity.page = index / PER_PAGE
        FullViewPagerActivity.index = index % PER_PAGE
        startActivity(intentFullViewPager)
    }
    override fun onReachRecyclerEnd() {
        searchPhotos(page++)
    }

    companion object{
        var location: LatLng? = LatLng(37.0,37.0)
    }
}
