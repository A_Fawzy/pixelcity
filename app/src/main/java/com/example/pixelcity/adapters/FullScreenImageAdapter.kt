package com.example.pixelcity.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pixelcity.models.Photo
import com.example.pixelcity.screens.fullVPager.FSImageListViewMvc
import com.example.pixelcity.screens.fullVPager.FSImageListViewMvcImpl
import com.example.pixelcity.utilities.getFlikrImageUrl
import com.github.chrisbanes.photoview.PhotoView

class FullScreenImageAdapter(private val mContext: Context, private val items: ArrayList<Photo>, val itemClicked: (PhotoView)-> Unit) :
    RecyclerView.Adapter<FullScreenImageAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mViewMvc = FSImageListViewMvcImpl(LayoutInflater.from(mContext), parent)

        return ViewHolder(mViewMvc,itemClicked)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(items[position])

        Log.e("7ammo", "position: ${position}, url: ${items[position].getFlikrImageUrl()}")
    }


    fun appendItems(newItems: ArrayList<Photo>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: FSImageListViewMvcImpl,val viewClicked: (PhotoView)->Unit)
                                : RecyclerView.ViewHolder(view.getRootView()) {

        var viewMvc: FSImageListViewMvcImpl = view


        fun bindView(item: Photo) {
            viewMvc.bindImageResult(item.getFlikrImageUrl())
            viewClicked(viewMvc.getPhotoDrawable()!!)
        }
    }



}