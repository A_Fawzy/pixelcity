package com.example.pixelcity.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.pixelcity.models.Photo
import com.example.pixelcity.screens.searchResult.BotImgListViewMvcImpl
import com.squareup.picasso.Picasso

class SheetImagesAdapter(
    private val mContext: Context,
    private val items: ArrayList<Photo>,
    val itemClicked: (Int) -> (Unit)
) :
    RecyclerView.Adapter<SheetImagesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val mViewMvc = BotImgListViewMvcImpl(LayoutInflater.from(mContext), parent)
        return ViewHolder(mViewMvc)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val imageUrl = getImageURL(position)
        holder
            .viewMvc
            .bindImageResult(imageUrl)

        holder
            .viewMvc
            .getRootView().setOnClickListener {

                itemClicked(position)

            }
    }

    fun appendItems(newItems: ArrayList<Photo>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }


    private fun getImageURL(position: Int): String {
        val farm = items[position].farm
        val server = items[position].server
        val id = items[position].id
        val secret = items[position].secret
        return "https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg"
    }


    inner class ViewHolder(view: BotImgListViewMvcImpl) : RecyclerView.ViewHolder(view.getRootView()) {
        var viewMvc: BotImgListViewMvcImpl = view


    }
}//  https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
