package com.example.pixelcity.common

import java.util.*
import kotlin.collections.HashSet

open class BaseObservableViewMvc<ListenerType> : ObservableViewMvc<ListenerType>,
        BaseViewMvc(){
    private var listeners = HashSet<ListenerType>()

    override fun registerListener(listener: ListenerType) {
        listeners.add(listener)
    }

    override fun unregisterListener(listener: ListenerType) {
        listeners.remove(listener)
    }

    protected fun getListener() : Set<ListenerType> {
        return Collections.unmodifiableSet(listeners)
    }
}