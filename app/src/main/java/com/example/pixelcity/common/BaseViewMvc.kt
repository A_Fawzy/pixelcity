package com.example.pixelcity.common

import android.content.Context
import android.view.View

abstract class BaseViewMvc : ViewMvc {
    private lateinit var rootView: View

    protected fun setRootView(view: View) {
        this.rootView = view
    }

    override fun getRootView(): View {
        return rootView
    }

    protected fun getContext() : Context {
        return getRootView().context
    }

    protected fun <T : View> findViewById(id: Int) : T{
        return getRootView().findViewById(id)
    }

}