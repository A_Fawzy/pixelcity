package com.example.pixelcity.common

import android.view.View

interface ViewMvc{

    fun getRootView(): View
}