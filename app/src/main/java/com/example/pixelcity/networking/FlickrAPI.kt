package com.example.pixelcity.networking

import com.example.pixelcity.models.PhotoResponseModel
import com.example.pixelcity.utilities.API_KEY
import com.example.pixelcity.utilities.API_METHOD
import com.example.pixelcity.utilities.PER_PAGE
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrAPI{
    //?method=flickr.photos.search&api_key=35ab4be9e6966d365fb7a3248aa7330b&lat=37&lon=37&format=json&nojsoncallback=1
    @GET("rest/")
    fun searchPhotos(
        @Query("method") method: String ? = API_METHOD,
        @Query("api_key") api_key: String? = API_KEY,
        @Query("lat") lat: Float,
        @Query("lon") lon: Float,
        @Query("format") format: String? = "json",
        @Query("page") page: Int? = 1,
        @Query("per_page") perpage: Int? = PER_PAGE,
        @Query("nojsoncallback") isCallBack: Int = 1
    ) : Call<PhotoResponseModel>

    companion object {
        fun create() : FlickrAPI{
            return RetrofitClient.getClient()!!.create(FlickrAPI::class.java)
        }
    }
}