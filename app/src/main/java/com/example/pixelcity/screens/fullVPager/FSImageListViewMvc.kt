package com.example.pixelcity.screens.fullVPager


interface FSImageListViewMvc {

    fun bindImageResult(photoURL: String)
}