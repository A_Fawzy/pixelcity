package com.example.pixelcity.screens.fullVPager

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.pixelcity.R
import com.example.pixelcity.common.BaseObservableViewMvc
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso

class FSImageListViewMvcImpl(inflater: LayoutInflater, parent: ViewGroup?) : FSImageListViewMvc,
        BaseObservableViewMvc<FSImageListViewMvc>(){

    private  var image : PhotoView
    private var photoURL: String? = ""
    init{
        super.setRootView(inflater.inflate(R.layout.image_full_screen, parent, false))
        image = findViewById(R.id.FSList_ImageView)
        image.scaleType = ImageView.ScaleType.FIT_XY
    }

    override fun bindImageResult(photoURL: String) {
        this.photoURL = photoURL
        image.loadImageFromUrl(photoURL)
    }

    fun getPhotoDrawable() : PhotoView? {

        return image
    }
}




private fun ImageView.loadImageFromUrl(url : String) {
    Picasso
        .get()
        .load(url)
        .into(this)
}