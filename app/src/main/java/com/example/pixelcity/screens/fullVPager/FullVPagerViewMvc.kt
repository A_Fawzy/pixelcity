package com.example.pixelcity.screens.fullVPager

import android.graphics.drawable.Drawable

interface FullVPagerViewMvc {
    interface PagerListener {
        fun onReachRecyclerEnd()
        fun onRecyclerClicked(index: Int)

        fun onPhotoDownloadClicked(drawable: Drawable)
        fun onPhotoShareClicked(drawable: Drawable)
    }
}