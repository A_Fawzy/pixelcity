package com.example.pixelcity.screens.fullVPager

import android.graphics.drawable.Drawable
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pixelcity.R
import com.example.pixelcity.adapters.FullScreenImageAdapter
import com.example.pixelcity.common.BaseObservableViewMvc
import com.example.pixelcity.models.Photo
import com.example.pixelcity.utilities.EndlessRecyclerOnScrollListener
import com.github.chrisbanes.photoview.PhotoView

class FullVPagerViewMvcImpl(inflater: LayoutInflater, parent: ViewGroup?) : FullVPagerViewMvc,
    BaseObservableViewMvc<FullVPagerViewMvc.PagerListener>() {

    private lateinit var adapter: FullScreenImageAdapter
    private  var mSaveBtn: Button
    private  var mShareBtn: Button
    private var fSViewPager: RecyclerView
    private var linearLayoutManager: LinearLayoutManager
    private  var imageDrawable: Drawable ?= null

    init {
        super.setRootView(inflater.inflate(R.layout.activity_full_view_pager, parent, false))
        mSaveBtn = findViewById(R.id.saveBtn)
        mShareBtn = findViewById(R.id.shareBtn)
        fSViewPager = findViewById(R.id.FSRecyclerV)
        linearLayoutManager = LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false)
        fSViewPager.layoutManager = linearLayoutManager
        fSViewPager.setHasFixedSize(true)
        fSViewPager.addOnScrollListener(object : EndlessRecyclerOnScrollListener() {
            override fun onLoadMore() {
                for (listener in getListener()) {
                    listener.onReachRecyclerEnd()
                }
            }

        })

        mSaveBtn.setOnClickListener {
            for (listener in getListener()) {
                listener.onPhotoDownloadClicked(imageDrawable!!) }
        }
        mShareBtn.setOnClickListener {
            for(listener in getListener()) {
                listener.onPhotoShareClicked(imageDrawable!!) }
        }




    }


    fun bindRv(photos: ArrayList<Photo>, index: Int) {
        if (::adapter.isInitialized) {
            //saves state of recyclerScroll
            val recyclerViewState = fSViewPager.layoutManager?.onSaveInstanceState()
            //appends the new items to the adapter
            adapter.appendItems(photos)
            //restores the state
            fSViewPager.layoutManager?.onRestoreInstanceState(recyclerViewState)
        } else {
            adapter = FullScreenImageAdapter(getContext(), photos) { photoView ->
                this.imageDrawable = photoView.drawable
            }
            fSViewPager.scrollToPosition(index)
        }
        fSViewPager.adapter = adapter
    }


}