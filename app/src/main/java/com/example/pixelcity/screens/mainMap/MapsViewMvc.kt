package com.example.pixelcity.screens.mainMap

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

interface MapsViewMvc {
    interface MapsListener{
        fun onMapClicked(latLng: LatLng)
        fun onPlaceSelected(latLng: LatLng, title: String)
        fun onMarkerClicked(marker: Marker)
    }
}