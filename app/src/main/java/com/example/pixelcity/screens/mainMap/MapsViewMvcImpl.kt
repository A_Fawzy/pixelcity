package com.example.pixelcity.screens.mainMap

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.pixelcity.R
import com.example.pixelcity.common.BaseObservableViewMvc
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions

class MapsViewMvcImpl (inflater: LayoutInflater, parent: ViewGroup?) : MapsViewMvc, OnMapReadyCallback,
        BaseObservableViewMvc<MapsViewMvc.MapsListener>(){

    private lateinit var mMap: GoogleMap
    private var location : LatLng? = null
    init{
        super.setRootView(inflater.inflate(R.layout.activity_maps, parent, false))
        (getContext() as AppCompatActivity).supportActionBar?.hide()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment =
            (getContext() as AppCompatActivity).supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val autoCompleteFragment = (getContext() as AppCompatActivity).supportFragmentManager.findFragmentById(R.id.place_autocomplete_fragment) as PlaceAutocompleteFragment?

        autoCompleteFragment?.setOnPlaceSelectedListener(object: PlaceSelectionListener{
            override fun onError(p0: Status?) {

            }

            override fun onPlaceSelected(place: Place?) {
                for(listener in getListener()){ listener.onPlaceSelected(place?.latLng!!, place.name?.toString()!!) }
            }

        })

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapClickListener { latLng ->
//            val latLng = LatLng(37.0, 37.0)
            val latLng = LatLng(64.2904,-20.912838)
            location = latLng
            for(listener in getListener()){ listener.onMapClicked(latLng) }
        }
    }

    fun dropPin(latLng: LatLng, title: String? = "Current Pin"){
        mMap.clear()
        val markerOptions = MarkerOptions().title(title).position(latLng)
        mMap.addMarker(markerOptions)
        mMap.setOnMarkerClickListener { marker0 ->
            for(listener in getListener()){ listener.onMarkerClicked(marker0!!) }
            false
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap.animateCamera(CameraUpdateFactory.zoomIn())
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(9f), 3000, null)
        Log.e("Map","Latlng = $latLng")
    }

}