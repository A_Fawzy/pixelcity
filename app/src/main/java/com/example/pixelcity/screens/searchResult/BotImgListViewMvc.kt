package com.example.pixelcity.screens.searchResult

interface BotImgListViewMvc {
    fun bindImageResult(photoURL: String)
}