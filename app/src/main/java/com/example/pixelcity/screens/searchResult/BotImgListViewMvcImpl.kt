package com.example.pixelcity.screens.searchResult

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.example.pixelcity.R
import com.example.pixelcity.common.BaseObservableViewMvc
import com.squareup.picasso.Picasso

class BotImgListViewMvcImpl(inflater: LayoutInflater, parent: ViewGroup?) : BotImgListViewMvc,
        BaseObservableViewMvc<BotImgListViewMvc>(){

    private  var image : ImageView
    private var photoURL: String? = ""
    init{
        super.setRootView(inflater.inflate(R.layout.bottom_image_list, parent, false))
        image = findViewById(R.id.bottom_RecImage)
    }

    override fun bindImageResult(photoURL: String) {
        this.photoURL = photoURL
        image.loadImageFromUrl(photoURL)

    }

}

private fun ImageView.loadImageFromUrl(url : String) {
    Picasso
        .get()
        .load(url)
        .into(this)
}