package com.example.pixelcity.screens.searchResult

interface SearchResultViewMvc {
    interface SearchResultListener{
        fun onReachRecyclerEnd()
        fun onRecyclerClicked(index: Int)
    }
}