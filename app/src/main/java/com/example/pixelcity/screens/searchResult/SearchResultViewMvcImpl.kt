package com.example.pixelcity.screens.searchResult

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pixelcity.R
import com.example.pixelcity.adapters.SheetImagesAdapter
import com.example.pixelcity.common.BaseObservableViewMvc
import com.example.pixelcity.models.Photo
import com.example.pixelcity.utilities.EndlessRecyclerOnScrollListener

class SearchResultViewMvcImpl (inflater: LayoutInflater, parent:ViewGroup?): SearchResultViewMvc,
        BaseObservableViewMvc<SearchResultViewMvc.SearchResultListener>(){
    private lateinit var adapter: SheetImagesAdapter
    private var mSearchResultRv: RecyclerView
    private var gridLayoutManager: GridLayoutManager
    private var index: Int? = 0
    init{
        super.setRootView(inflater.inflate(R.layout.activity_search_result, parent, false))
        mSearchResultRv = findViewById(R.id.photosResultRv)
        gridLayoutManager = GridLayoutManager(getContext(), 3)
        mSearchResultRv.layoutManager = gridLayoutManager
        mSearchResultRv.setHasFixedSize(true)
        mSearchResultRv.addOnScrollListener(object: EndlessRecyclerOnScrollListener(){

            override fun onLoadMore() {
                for(listener in getListener()){ listener.onReachRecyclerEnd() }
            }

        })
    }
    fun bindRv(photos: ArrayList<Photo>){
        if(::adapter.isInitialized){
            //saves state of recyclerScroll
            val recyclerViewState = mSearchResultRv.layoutManager?.onSaveInstanceState()
            //appends the new items to the adapter
            adapter.appendItems(photos)
            //restores the state
            mSearchResultRv.layoutManager?.onRestoreInstanceState(recyclerViewState)
        }
        else
                adapter = SheetImagesAdapter(getContext(), photos){index ->
                        for (listener in getListener()) {
                            listener.onRecyclerClicked(index)
                        }

                this.index = index
                Toast.makeText(getContext(), index.toString(), Toast.LENGTH_LONG).show()
            }
        mSearchResultRv.adapter = adapter
    }
}