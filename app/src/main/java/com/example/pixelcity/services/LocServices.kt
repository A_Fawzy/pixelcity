package com.example.pixelcity.services

import com.example.pixelcity.models.PhotoResponseModel
import com.example.pixelcity.networking.FlickrAPI
import com.example.pixelcity.utilities.API_KEY
import com.example.pixelcity.utilities.API_METHOD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object LocServices{
    private var retroClient: FlickrAPI? = null

    fun searchPhotos(lat: Float?, lon : Float?, page:Int? = 1, completion : (model: PhotoResponseModel?, error: String?) -> Unit){
        retroClient = FlickrAPI.create()
        retroClient!!.searchPhotos(API_METHOD, API_KEY, lat!!, lon!!, page = page) .enqueue(object : Callback<PhotoResponseModel> {
            override fun onFailure(call: Call<PhotoResponseModel>, t: Throwable) {
                completion(null, t.localizedMessage)
            }

            override fun onResponse(call: Call<PhotoResponseModel>, response: Response<PhotoResponseModel>) {
                if(response.isSuccessful) {
                    if (response.body()?.stat == "ok")
                        completion(response.body()!!, null)
                    else
                        completion(null, response.message())
                }
            }

        })
    }
}