package com.example.pixelcity.utilities

import com.example.pixelcity.models.Photo

fun Photo.getFlikrImageUrl() : String {//  https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg

    return "https://farm${this.farm}.staticflickr.com/${this.server}/${this.id}_${this.secret}.jpg"
}